module github.com/my/repo

go 1.13

require (
	github.com/go-redis/redis/v7 v7.0.0-beta.5 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
)
